# How to setup qemu for Cortex-A9 with A7 ARM


* ## [Linux installation](#setup-a-linux-working-environment)
* ## [Windows installation](#setup-a-windows-working-environment)

## Setup a linux working environment

we are going to use qemu over a linux environment, I'll be using ubuntu 20.04 for the examples here.

for that we will need to install the following package: 

1. `qemu`
2. `qemu-user`
3. `qemu-utils`
3. `qemu-arm-static`
4. `qemu-system-arm`
5. `qemu-efi-arm`
6. `qemu-kvm`

(some of them might be redundant).

After installing all of the above package on your machine via your package manager (eg. apt, apt-get, yum) (on ubuntu its `sudo apt install <PACKAGE_NAME>` ), you souls be able to run the `qemu-system-arm` command.

if you are running the emulator on this machine you can skip to the [next chapter](#running-qemu).


if you are running the emulator on an offline machine you will need to install the above packages on that machine as well.

on ubuntu you can do it by fowliang those steps:

1. Make sure your offline machine is running the same version of ubuntu(eg. 10.10 12.04, etc) and have the same machine architecture(eg. x84, x64)

2. use apt to download all of your installed packages on the online machine to `/var/cache/apt/archives` by running: `dpkg -l | grep "^ii"| awk ' {print $2} ' | xargs sudo apt-get -y --force-yes install --reinstall --download-only` , this is going to take some time and might install a lot of redundant packages but its okay, if you want to install other package that you are missing on your offline machine like `vim`( good text editor ) and  `open-vm-tools-desktop` ( allow clip bored and drag and drop form inside a ghast machine on vm-ware).

3. create a tar archive from `/var/cache/apt/archives` by running the following commands `cd /var/cache/apt` and then `tar -zcvf <FULL_PATH_TO_NEW_TAR_FILE>  archives/*`.

4. copy that tar file to your offline computer and extract the tar by running `tar -zxvf <YOUR_TAR_FILE>` anywhere on your machine.

5. install all the .deb files inside of the `archives` directory that you just extracted by running: `sudo dpkg -i ./archives/*deb`.

Thats it you are have qemu on your offline machine! you can go to the [next chapter](#running-qemu).

## Setup a windows working environment

For working with qemu on windows we need only to install qmue with the qemu installer that can be [found here.](https://qemu.weilnetz.de/w64/)

After installing the installer you will need to add the path you selceted in the installer(eg. `C:\Program Files\qemu`) to your PATH envirment varubale.

Thats it you can move to the [next chapter](#running-qemu).

## Running qemu 

After qemu and qemu support for arm you can start using the machine.

first of all you will need to download the fowling:

1. an arm compiled linux kernel
2. ca-9 Device tree blob 
3. and the file system image

all of those files are stored inside this [tar file](https://drive.google.com/file/d/1OI6gfSwNmMA7XEvgj5_DLHk52iX6jk2d/view?usp=drivesdk), !!THIS TAR IS DECOMPRESS TO AROUND 8.5 GB OF DATA!! make sure you have that kind of free disk space.

after decompressing that tar file you can execute the following command to run you emulated board on linux : 
```
qemu-system-arm -m 1024M -sd armdisk.img \
        -dtb vexpress-v2p-ca9.dtb \
        -M vexpress-a9 -cpu cortex-a9 \
        -kernel zImage \
        -append "root=/dev/mmcblk0p2" \
        -net nic -net user,hostfwd=tcp::2222-:22 \
        -no-reboot
```

or this command on windows:
```
qemu-system-arm -m 1024M -sd armdisk.img^
        -dtb vexpress-v2p-ca9.dtb^
        -M vexpress-a9 -cpu cortex-a9^
        -kernel zImage^
        -append "root=/dev/mmcblk0p2"^
        -net nic -net user,hostfwd=tcp::2222-:22^
        -no-reboot
```

after that a black screen will show up and the machine will start to boot.

the user name of that machine is: `user` and the password is: `user`, if you need to become sudo you can run `su` and then enter the root password: `root`.

you can export any port as a port forward to the host machine by adding `hostfwd=<tcp/udp>::<HOST_PORT>-:<GUEST_PORT>` to the arg line `-net nic -net user,...` for exempla to port forward 22 to 2222 and 80 to 8888 you need to run :
```
qemu-system-arm -m 1024M -sd armdisk.img \
        -dtb vexpress-v2p-ca9.dtb \
        -M vexpress-a9 -cpu cortex-a9 \
        -kernel zImage \
        -append "root=/dev/mmcblk0p2" \
        -net nic -net user,hostfwd=tcp::2222-:22,hostfwd=tcp::8888-:80 \
        -no-reboot
```
